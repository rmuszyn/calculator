package Tests;

import Calculator.Calculator;
import org.junit.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseTest {

    WebDriver driver;

    @Before
    public void setup(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("http://web2.0calc.com/");
        Calculator calc = new Calculator(driver);
        try {
            calc.consent();
        } catch (NoSuchElementException e) {
            System.out.println("No pop-up to click.");
        }
    }

    @Test
    public void test() {
        try {
            Calculator calc = new Calculator(driver);

            calc.putInput("35*999+(100/4)");
            calc.equal();
            Thread.sleep(500);
            Assert.assertEquals(calc.getInputInt(), 34990);

            calc.clear();
            calc.radClick();
            calc.cosClick();
            calc.piClick();
            calc.equal();
            Thread.sleep(500);
            Assert.assertEquals(calc.getInputInt(), -1);

            calc.clear();
            calc.eightClick();
            calc.oneClick();
            calc.sqrtClick();
            calc.equal();
            Thread.sleep(500);
            Assert.assertEquals(calc.getInputInt(), 9);

            calc.historyClick();
            Assert.assertEquals(calc.getHistorySize(), 3);
            Assert.assertEquals(calc.getPosFromHistory(1), "sqrt(81)");
            Assert.assertEquals(calc.getPosFromHistory(2), "cos(pi");
            Assert.assertEquals(calc.getPosFromHistory(3), "35*999+(100/4)");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @After
    public void endTest() {
        if(driver != null) {
            driver.quit();
        }
    }
}
