package Calculator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class Calculator {

    WebDriver driver;

    @FindBy(className = "fc-button-label")
    WebElement consentBtn;

    @FindBy(className = "r")
    private List<WebElement> historyField;

    @FindBy(id = "BtnClear")
    private WebElement clearBtn;

    @FindBy(id = "BtnCos")
    private WebElement cosBtn;

    @FindBy(id = "BtnPi")
    private WebElement piBtn;

    @FindBy(id = "BtnCalc")
    private WebElement equalBtn;

    @FindBy(id = "BtnSqrt")
    private WebElement sqrtBtn;

    @FindBy(id = "Btn1")
    private WebElement oneBtn;

    @FindBy(id="Btn8")
    private WebElement eightBtn;

    @FindBy(xpath = "//*[@id=\"input\"]")
    private WebElement inputField;

    @FindBy(xpath = "//*[@id=\"trigorad\"]")
    private WebElement radBtn;

    @FindBy(xpath = "//*[@id=\"hist\"]/button[2]")
    private WebElement historyBtn;


    public Calculator(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void consent() {
        consentBtn.click();
    }

    public void clear() {
        clearBtn.click();
    }

    public void equal() {
        equalBtn.click();
    }

    public void sqrtClick(){
        sqrtBtn.click();
    }

    public void cosClick() {
        cosBtn.click();
    }

    public void radClick() {
        radBtn.click();
    }

    public void piClick() {
        piBtn.click();
    }

    public void oneClick() {
        oneBtn.click();
    }

    public void eightClick() {
        eightBtn.click();
    }

    public void historyClick() {
        historyBtn.click();
    }

    public void putInput(String input){
        inputField.sendKeys(input);
    }

    public int getInputInt() {
        int sum = Integer.parseInt(inputField.getAttribute("value"));
        System.out.println(sum);
        return sum;
    }

    public int getHistorySize() {
        int histSize = historyField.size();
        System.out.println(histSize);
        return histSize;
    }

    public String getPosFromHistory(int posNumber) {
        String posValue = driver.findElement(By.xpath(String.format("//*[@id=\"histframe\"]/ul/li[%d]/p[2]", posNumber))).getAttribute("title");
        System.out.println(posValue);
        return posValue;
    }

}
